/* eslint-disable */
import React from 'react'
import { mount, shallow } from 'enzyme'
import { expect } from 'chai'
import LocationCard from './../src/components/LocationCard'

describe('<LocationCard/>', function () {
  it('should have a header and a card container div', function () {
    const wrapper = shallow(<LocationCard/>)
    expect(wrapper.find('div')).to.have.length(2)
  })
  it('should have a table to display location information', function () {
    const wrapper = shallow(<LocationCard/>)
    expect(wrapper.find('table')).to.have.length(1)
  })

  it('should have props for title website and userLocation', function () {
    const wrapper = shallow(<LocationCard/>)
    expect(wrapper.props().title).to.equal(undefined)
    expect(wrapper.props().website).to.equal(undefined)
    expect(wrapper.props().userLocation).to.equal(undefined)
  })
})
