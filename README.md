# Avenue Code UI Challenge - Gustavo Garcia

## Installation
clone this repository, then enter the project folder and simply type:

*This will install all necessary modules and dependencies*
```code 
yarn
```
> Note: Be sure to have installed npm and yarn as global dependencies.

## Running
In order to run this project simply type:

*Start geolocation application in development mode*
```code 
npm start
```

This will automatically open a tab in your browser with the running application.
Enjoy it!


## Testing
In order to run the tests covered in the project, type:

*Run tests using enzyme, mocha an chai*
```code 
npm test
```


## Project Structure:
Here`s how I organized this project

* Folder [test](test): The folder contain the Units Tests.
* Folder [src](src): The files with application context and logic.
* Folder [assets](assets): All files used as external libraries or fonts in this project.

## Technologies and Frameworks

In order to achieve the goals of this challenge, I used the following technologies/frameworks

### ReactJs

[ReactJS](https://reactjs.org/) is a declarative, component-based javascript framework created by [Facebook](https://facebook.com) team which aims to make easier to build user interfaces.
Some cool facts abaout ReactJS, are that it carries some interesting concepts when building web applications, and even mobile applications with React Native, as the unidirectional flow, state monitoring and reiability using the certain tools.

Because of these facts and others I chose ReactJS as the main framework building this application.

Also, the community is growing up so fast, there are already thousands of libraries and components ready to be used out there, which mean less trouble doing the hard work and more focus on your business.

### Foundation
As the interface framework, I chose [Foundation](https://foundation.zurb.com/). Not much to say about Foundation. As many others ui frameworks Foundation provides a lot of visual stylized components, which make it a lot easier to build your application with a lean and responsive layout.

### Sass
[SASS](http://sass-lang.com/) was my first choice for a pre processor. I've been usng it for a while and it helps me a lot when building application with custom colors referring to the brand, for an example. Using SASS you can define all colors that represent a brand or your client in one file, and use it whenever and wherever you need in your entire application.

There are a lot more useful utilities we can use SASS for. This was just an example of how it can be handy when creating styles for your application.

### Enzyme
[Enzyme](http://airbnb.io/enzyme/) is a Javascript Testing utilitis for React created by [AirBnB](https://airbnb.io/) development team. It is a great test tool which provides methods out of the box so you can test yout React components without going too much trouble. In order to work with Enzyme written tests, I used [Mocha](https://mochajs.org/) as a test runner, an [Chai](http://www.chaijs.com/) as an assertion library.

For any suggestions please contact-me at gustavohgs13@gmail.com

