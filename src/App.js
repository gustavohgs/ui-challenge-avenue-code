import React, { Component } from 'react'
import { Provider } from 'react-redux'
import Home from './pages/Home'
import Toast from './components/Toast'
import store from './store'
import styles from './app.scss'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      test: '',
    }
  }
  render() {
    return (
      <Provider store={store}>
        <div className={styles.main}>
          <h1 className={styles.headerContainer}>GeoLocation</h1>
          <Home />
          <Toast />
        </div>
      </Provider>
    )
  }
}
