import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.scss'

const LocationCard = (props) => {
  const { userLocation } = props
  return (
    <div className={`${styles.containerCard} card`}>
      <div className={styles.cardHeader}>
        {props.title}
      </div>
      <table>
        <tbody>
          <tr>
            <td>IP</td>
            <td>{userLocation.ip}</td>
          </tr>
          <tr>
            <td>Hostname</td>
            <td>
              {
                props.webSite ? props.webSite : 'localhost'
              }
            </td>
          </tr>
          <tr>
            <td>Country</td>
            <td>{userLocation.country_code}</td>
          </tr>
          <tr>
            <td>State</td>
            <td>{userLocation.region_code}</td>
          </tr>
          <tr>
            <td>Latitude</td>
            <td>{userLocation.latitude}</td>
          </tr>
          <tr>
            <td>Longitude</td>
            <td>{userLocation.longitude}</td>
          </tr>
          <tr>
            <td>Time zone</td>
            <td>{userLocation.time_zone}</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

LocationCard.propTypes = {
  userLocation: PropTypes.object,
  title: PropTypes.string.isRequired,
  webSite: PropTypes.string,
}

LocationCard.defaultProps = {
  webSite: null,
  userLocation: {
    ip: '',
    country_code: '',
    region_code: '',
    latitude: '',
    longitude: '',
    time_zone: '',
  },
}

export default LocationCard
