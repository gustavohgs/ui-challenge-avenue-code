import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ToastContainer, toast } from 'react-toastify'
import PropTypes from 'prop-types'

class Toast extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.showNotification) {
      this.handleShowNotification(nextProps.showNotification)
    }
  }
  handleShowNotification = (data) => {
    const notificationObj = {
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: 2500,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      draggablePercent: 60,
    }
    toast.warn(data.message || 'Oops! This is embarassing...', notificationObj)
  }
  render() {
    return (
      <ToastContainer />
    )
  }
}

Toast.propTypes = {
  showNotification: PropTypes.object,
}


Toast.defaultProps = {
  showNotification: null,
}

const mapStateToProps = state => ({
  showNotification: state.application.showApplicationWarning,
})

export default connect(mapStateToProps)(Toast)
