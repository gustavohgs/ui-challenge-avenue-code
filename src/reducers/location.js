import * as types from '../actions/actionTypes'

const initialState = {
  userLocation: null,
  websiteLocations: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_MY_LOCATION:
      return { ...state, userLocation: payload }
    case types.FETCH_WEBSITE_LOCATION:
      return { ...state, websiteLocation: payload }
    default:
      return state
  }
}
