import location from './location'
import application from './application'

export default {
  application,
  location,
}
