import * as types from '../actions/actionTypes'

const initialState = {
  isLocationLoading: null,
  showApplicationWarning: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.IS_LOCATION_LOADING:
      return { ...state, isLocationLoading: payload }
    case types.SHOW_APPLICATION_WARNING:
      return { ...state, showApplicationWarning: payload }
    default:
      return state
  }
}
