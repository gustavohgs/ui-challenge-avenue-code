import { createStore, combineReducers, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger' // eslint-disable-line
import thunk from 'redux-thunk'
import reducers from './reducers'

const store = createStore(
  combineReducers({
    ...reducers,
  }),
  applyMiddleware(
    createLogger(),
    thunk,
  ),
)

export default store
