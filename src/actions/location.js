import { createAction } from 'redux-actions'
import * as types from './actionTypes'
import LocationService from '../services/LocationService'


export const fetchMyLocation = () => (
  (dispatch) => {
    LocationService
      .fetchMyLocation()
      .then((data) => {
        console.log('my location data', data)
        dispatch(createAction(types.FETCH_MY_LOCATION)(data))
        return null
      })
      .catch(err =>
        dispatch(createAction(types.SHOW_APPLICATION_WARNING)({ message: err.toString() })))
    return null
  }
)

export const fetchWebsiteLocation = url => (
  (dispatch) => {
    LocationService
      .fetchWebsiteLocation(url)
      .then((siteLocation) => {
        if (siteLocation && siteLocation.ip) {
          return dispatch(createAction(types.FETCH_WEBSITE_LOCATION)(siteLocation))
        }
        return null
      })
      .catch(err =>
        dispatch(createAction(types.SHOW_APPLICATION_WARNING)({ message: err.toString() })))
    return null
  }
)
