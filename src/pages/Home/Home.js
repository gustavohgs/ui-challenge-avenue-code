import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import LocationCard from '../../components/LocationCard'
import {
  fetchMyLocation,
  fetchWebsiteLocation,
} from '../../actions/location'
import { urlPattern } from '../../utils/inputPatterns'
import styles from './styles.scss'


class Home extends Component {
  state = {
    webUrl: '',
    userLocation: null,
    websiteLocation: null,
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      if (nextProps.userLocation !== this.state.userLocation) {
        this.setState({
          userLocation: nextProps.userLocation,
        })
      }
      if (nextProps.websiteLocation !== this.state.websiteLocation) {
        this.setState({
          websiteLocation: nextProps.websiteLocation,
        })
      }
    }
  }
  handleUrlChange = (e) => {
    this.setState({
      webUrl: e.target.value,
    })
  }

  handleMyLocationClick = () => {
    this.props.fetchMyLocation()
  }
  handleUrlClick = (e) => {
    e.preventDefault()
    this.props.fetchWebsiteLocation(this.state.webUrl)
  }
  resetUserLocation = () => {
    this.setState({
      userLocation: null,
    })
  }
  renderUserLocation = () => {
    if (this.state.userLocation) {
      const { userLocation } = this.state
      return (
        <LocationCard userLocation={userLocation} title="My location" />
      )
    }
    return null
  }
  renderWebsiteLocation = () => {
    if (this.state.websiteLocation) {
      const { websiteLocation } = this.state
      return (
        <LocationCard
          userLocation={websiteLocation}
          title="Website location"
          webSite={this.state.webUrl}
        />
      )
    }
    return null
  }

  render() {
    return (
      <div className={styles.container}>
        <div className="grid-x">
          <form
            className={`${styles.siteFormContainer} cell small-12 medium-6 shrink`}
            onSubmit={this.handleUrlClick}
          >
            <input
              type="text"
              placeholder="Ex.: www.example.com"
              onChange={this.handleUrlChange}
              value={this.state.webUrl}
              className={styles.formElement}
              pattern={urlPattern.pattern}
              title={urlPattern.errorMessage}
              required
            />
            <button
              className={`${styles.formElement} button`}
              type="submit"
            >
              Locate
            </button>
          </form>
          <button
            className="button success cell small-12 medium-6 shrink"
            style={{ flex: 1, margin: 8, color: '#fff' }}
            onClick={this.handleMyLocationClick}
          >
            {"What's my location?"}
          </button>
          <button
            className="button secondary cell small-12 medium-6 shrink"
            style={{ flex: 1, margin: 8 }}
            onClick={this.resetUserLocation}
          >
            Reset my location
          </button>
        </div>
        <div className="grid-x">
          <div className="cell small-12 medium-6 shrink">
            {this.renderWebsiteLocation()}
          </div>
          <div className="cell small-12 medium-6 shrink">
            {this.renderUserLocation()}
          </div>
        </div>
      </div>
    )
  }
}

Home.propTypes = {
  fetchMyLocation: PropTypes.func,
  fetchWebsiteLocation: PropTypes.func,
  userLocation: PropTypes.object,
  websiteLocation: PropTypes.object,
}

Home.defaultProps = {
  fetchMyLocation: () => null,
  fetchWebsiteLocation: () => null,
  userLocation: null,
  websiteLocation: null,
}

const mapStateToProps = state => ({
  userLocation: state.location.userLocation,
  websiteLocation: state.location.websiteLocation,
})

const mapActionsToProps = dispatch => ({
  fetchMyLocation() {
    dispatch(fetchMyLocation())
  },
  fetchWebsiteLocation(url) {
    dispatch(fetchWebsiteLocation(url))
  },
})

export default connect(mapStateToProps, mapActionsToProps)(Home)
