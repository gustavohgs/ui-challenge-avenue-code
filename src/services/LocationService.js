import api from './api'

const LocationService = {
  async fetchMyLocation() {
    const res = await api.get('/')
    return res.data
  },
  async fetchWebsiteLocation(website) {
    const res = await api.get(`/${website}`)
    return res.data
  },
}

export default LocationService
