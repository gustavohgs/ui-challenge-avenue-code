import { create } from 'apisauce'
import { createAction } from 'redux-actions'
import Store from '../store'
import * as types from '../actions/actionTypes'

const baseURL = 'http://freegeoip.net/json'
const api = create({
  baseURL,
  headers: {},
})

api.axiosInstance.interceptors.response.use(response => (
  response
), (error) => {
  if (error && error.response && error.response.status === 200) {
    return Promise.resolve(error.response.data)
  }
  Store.dispatch(createAction(types.SHOW_APPLICATION_WARNING)({
    message: error.message
    || error.response.data,
  }))
  return Promise.reject(error)
})


export default api
