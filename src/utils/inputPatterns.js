export const urlPattern = {
  errorMessage: 'Url must be in the following format: www.example.com or similar',
  pattern: '^(www.)[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$',
}

export default urlPattern
